import React, { Component } from 'react';
import './App.css';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			fields: {
				name: '',
				email: '',
				password: '',
				selectedState: '',
				remember_me: false
			},
			errors: {},		
			states: [
				{key: 'Oregon', value: 1 },
				{key: 'Washington', value: 2 },
				{key: 'Colorado', value: 3 },
			],			
		};

		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleInputChange(e) {
		const target = e.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;

		this.setState(prevState => ({
			fields: {
				...prevState.fields,
				[name]: value
			}
		}));
	}

	handleSubmit(e) {
		this.validateForm();
		console.log(JSON.parse(JSON.stringify(this.state.fields.name)));
		e.preventDefault();
	}

	validateForm() {
		let fields = this.state.fields;
		let errors = {};
		let formIsValid = true;

		if (!fields['name'].trim()) {
			formIsValid = false;
			errors["name"] = "*Please enter your name.";
		}

		if (typeof fields["email"] !== "undefined") {
			//regular expression for email validation
			var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
			
			if (!pattern.test(fields["email"])) {
				formIsValid = false;
				errors["email"] = "*Please enter a valid email.";
			}
		}

		if (!fields["password"]) {
			formIsValid = false;
			errors["password"] = "*Please enter your password.";
		}

		if (fields["selectedState"] === '') {
			formIsValid = false;
			errors["selectedState"] = "*Please select a state.";
		}

		this.setState({
			errors
		});

		return formIsValid;
	}

	render() {
		return (
			<div className="App">
				<div className="container">
					<div className="row mb-5">
						<div className="col">
							<h1>
								React.js Forms
							</h1>
						</div>
					</div>
					<form onSubmit={this.handleSubmit}>
						<div className="form-group">
							<label className="d-none mb-2" for="name">
								Name
							</label>
							<input 
								type="text"
								name="name" 
								className="form-control" 
								id="name" 
								aria-describedby="nameHelp" 
								placeholder="Name"
								value={this.state.fields.name}
								onChange={this.handleInputChange}
							/>
							<div className="errorMsg">
								{this.state.errors.name}
							</div>
  						</div>
						<div className="form-group">
							<label className="d-none mb-2" for="Email">
								Email
							</label>
							<input 
								type="email"
								name="email" 
								className="form-control" 
								id="Email" 
								aria-describedby="emailHelp" 
								placeholder="Enter email"
								value={this.state.fields.email}
								onChange={this.handleInputChange}
							/>
							<div className="errorMsg">
								{this.state.errors.email}
							</div>
  						</div>
						<div className="form-group">
							<label className="d-none mb-2" for="Password">
								Password
							</label>
							<input 
								type="password" 
								name="password"
								className="form-control" 
								id="Password" 
								placeholder="Password" 
								value={this.state.fields.password.trim()}
								onChange={this.handleInputChange}
							/>
							<div className="errorMsg">
								{this.state.errors.password}
							</div>
						</div>
						<div className="form-group">
							<label className="d-none mb-2" for="States">
								State
							</label>
							<select 
								className="custom-select"
								name="selectedState"
								value={this.state.fields.selectedState}
								onChange={this.handleInputChange}
							>
								<option value="" disabled hidden>
									Please select a state...
								</option>
								{this.state.states.map((state) => <option key={state.value} value={state.value}>{state.key}</option>)}
							</select>
							<div className="errorMsg">
								{this.state.errors.selectedState}
							</div>
						</div>
						<div className="form-group form-check">
							<input 
								type="checkbox"
								name="remember_me" 
								className="form-check-input" 
								id="remember_me" 
								onChange={this.handleInputChange}
								checked={this.state.remember_me}
							/>
							<label className="form-check-label" for="remember_me">
								Remember me
							</label>
						</div>
						<div className="row justify-content-center">
							<div className="col-6">
								<button type="submit" className="btn btn-primary custom-btn mt-5 btn-block">
									Submit
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		);
	}
}

export default App;
